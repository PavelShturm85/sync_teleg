import os
import time
from telethon import TelegramClient, utils
import pymongo
from pymongo import MongoClient
from fuzzywuzzy import fuzz
import regex
import difflib


class MongoClientForTelegram():
    SETTINGS = dict(
        mongoIP="127.0.0.1",
        mongoPort=27017,
    )

    def __init__(self, mongoIP=SETTINGS["mongoIP"], mongoPort=SETTINGS["mongoPort"]):
        mongo = MongoClient(mongoIP, mongoPort, connect=False)
        self.telegram_mess_db = mongo.telegram_mess_db


class InitTelegramClient():
    SETTINGS = dict(
        api_id=,
        api_hash='',
        phone='+7',
        session_name='template',
    )

    def __init__(self, session_name, api_id=SETTINGS['api_id'], api_hash=SETTINGS['api_hash'], phone=SETTINGS['phone']):
        self.session_name = session_name
        self.api_hash = api_hash
        self.api_id = api_id
        self.phone = phone
        self.client = TelegramClient(
            self.session_name, self.api_id, self.api_hash)

    def __make_session_db(self):
        file_name = f"{self.session_name}.session"
        if not os.path.isfile(file_name):
            command = f"cp ./template.session ./{file_name}"
            os.system(command)

    def start(self):
        self.__make_session_db()
        self.client.start(phone=self.phone)
        return self.client

    def stop(self):
        self.client.disconnect()


class SearchWordInMessage():

    @staticmethod
    def is_word_in_black_list_regex(message, black_list):

        phrase_in_text = []
        message = message.lower()
        for phrase in black_list:
            phrase = phrase.lower()
            split_phrase = phrase.split()
            count = 0
            for word in split_phrase:

                len_word = len(word)
                if 6 >= len_word >= 4:
                    accuracy = 1
                elif 9 >= len_word > 6:
                    accuracy = 2
                elif len_word > 9:
                    accuracy = 3
                else:
                    accuracy = 0

                accuracy_template = "{e<=%s}" % accuracy
                template_search = f'({word}){accuracy_template}'
                regex_search = regex.search(
                    template_search, message, flags=regex.IGNORECASE)
                if regex_search:
                    count += 1

            if count >= len(split_phrase):
                phrase_in_text.append(phrase)
        return tuple(phrase_in_text)

    @staticmethod
    def is_word_in_black_list_difflib(message, black_list):

        phrase_in_text = []
        message = message.lower()
        for phrase in black_list:
            phrase = phrase.lower()
            split_phrase = phrase.split()
            count = 0
            for word in split_phrase:
                for mess_w in message.split():
                    d = difflib.SequenceMatcher(None, mess_w, word).ratio()
                    if d > 0.65:
                        count += 1

            if count >= len(split_phrase):
                phrase_in_text.append(phrase)
        return tuple(phrase_in_text)

    @staticmethod
    def is_word_in_black_list_fuzzy(message, black_list):

        phrase_in_text = []
        message = message.lower()
        for phrase in black_list:
            phrase = phrase.lower()
            split_phrase = phrase.split()
            count = 0
            for word in split_phrase:
                for mess_w in message.split():
                    f = fuzz.ratio(mess_w, word)
                    if f >= 65:
                        count += 1

            if count >= len(split_phrase):
                phrase_in_text.append(phrase)
        return tuple(phrase_in_text)


class TelegramMessageCollector(MongoClientForTelegram):

    def __init__(self, chat_id):
        super().__init__()

        self.chat_id = chat_id.replace('@', '')
        self.chat = self.telegram_mess_db[self.chat_id]

        self.telegram_client = InitTelegramClient(self.chat_id)
        self.client = self.telegram_client.start()

    def __save_messages(self, entity):

        for message in self.client.iter_messages(entity):
            is_mess_in_db = self.chat.find_one({"mess_id": message.id})
            is_first_mess_in_db = self.chat.find_one({"mess_id": 1})
            if is_mess_in_db and is_first_mess_in_db:
                break
            elif is_mess_in_db and not is_first_mess_in_db:
                print(f"in db:{message.id}")
                continue
            mess = message.message
            if isinstance(mess, str) and mess:

                message = {
                    "type": "message",
                    "name": utils.get_display_name(message.sender),
                    "name_id": message.from_id,
                    "mess_id": message.id,
                    "mess_date": message.date,
                    "message": mess,
                    "reply_to_msg_id": message.reply_to_msg_id,
                    "check_contact": False,
                    "check_black_word": False,
                }

                self.chat.insert_one(message)
                print(message)

    def start_collector(self):
        entity = self.client.get_entity(self.chat_id)
        self.__save_messages(entity)
        self.telegram_client.stop()


class FilterMessagesByBlackList(MongoClientForTelegram):

    BLACK_LIST = [ "путин", "навальный", "президент"]

    def __init__(self,
                 chat_id,
                 black_list=BLACK_LIST):
        super().__init__()
        self.black_list = black_list
        self.black_str = " ".join(self.black_list)
        self.chat_id = chat_id.replace('@', '')
        self.chat = self.telegram_mess_db[self.chat_id]
        self.chat = self.telegram_mess_db[self.chat_id]
        self.chat.create_index([('message', pymongo.TEXT)], name='text', default_language='russian')
        name_black_collect = f'black_{self.chat_id}'
        self.black_chat = self.telegram_mess_db[name_black_collect]
        self.count_mess_around_black_mess = 5
        chat_mess = self.chat.find({"type": "message"}, no_cursor_timeout=True)
        self.list_id_mess = [mess["mess_id"]
                             for mess in chat_mess][::-1]
        chat_mess.close()
        
    def __cleare_interval_list(self, intervals_list):
        cleare_interval_list = []
        for elements in intervals_list:
            for element in range(*elements):
                cleare_interval_list.append(element)
        return set(cleare_interval_list)

    def __get_correct_messages_id_for_save(self, mess_id_list):
        pre_id = None
        first_id_index = None
        last_id_index = None
        intervals_list = []
        reversed_mess_id_list = mess_id_list[::-1]
        last_mess_id_item = len(self.list_id_mess) - 1
        for message_id in reversed_mess_id_list:
            message_id_index = self.list_id_mess.index(message_id)
            if not last_id_index and not first_id_index:
                if message_id_index - self.count_mess_around_black_mess > 0:
                    first_id_index = message_id_index - self.count_mess_around_black_mess
                else:
                    first_id_index = 0

            if reversed_mess_id_list[-1] == message_id and not pre_id:
                if last_mess_id_item - message_id_index > self.count_mess_around_black_mess:
                    last_id_index = message_id_index + self.count_mess_around_black_mess
                else:
                    last_id_index = last_mess_id_item
            else:
                if not pre_id:
                    pre_id = message_id_index
                    continue

                if message_id_index - pre_id <= self.count_mess_around_black_mess:
                    if reversed_mess_id_list[-1] == message_id:
                        if last_mess_id_item - message_id_index > self.count_mess_around_black_mess:
                            last_id_index = message_id_index + self.count_mess_around_black_mess
                        else:
                            last_id_index = last_mess_id_item
                    else:
                        pre_id = message_id_index
                        continue
                else:
                    if last_mess_id_item - pre_id > self.count_mess_around_black_mess:
                        last_id_index = pre_id + self.count_mess_around_black_mess
                    else:
                        last_id_index = last_mess_id_item

            if first_id_index and last_id_index:
                intervals_list.append([first_id_index, last_id_index])
                if first_id_index != message_id_index:
                    first_id_index = None
                else:
                    first_id_index = message_id_index
                last_id_index = None
                pre_id = None
        return self.__cleare_interval_list(intervals_list)

    def __save_mess_id_with_black_keyword(self):
        chat = self.chat.find({"type": "message", "check_black_word": False, "$text": {"$search":self.black_str}}, no_cursor_timeout=True)
        
        for message in chat:
            mess = message['message']
            if mess:
                _regex = SearchWordInMessage.is_word_in_black_list_regex(
                    mess, self.black_list)
                _difflib = SearchWordInMessage.is_word_in_black_list_difflib(
                    mess, self.black_list)
                _fuzzy = SearchWordInMessage.is_word_in_black_list_fuzzy(
                    mess, self.black_list)
                if _regex and _difflib and _fuzzy and _regex == _difflib == _fuzzy:
                    keyword = _regex or _difflib or _fuzzy
                    mess_id = message['mess_id']
                    key = "#".join(keyword)

                    self.black_chat.update_one(
                        {"keyword": key, "black_word": True}, {'$push': {key: mess_id}}, upsert=True)
            self.chat.update_one(
                {'type': 'message', "mess_id": message["mess_id"]}, {
                    '$set': {"check_black_word": True}})
        chat.close()

    def __get_message_id_with_keyword_in_black_list(self):
        self.__save_mess_id_with_black_keyword()
        black_word_dicts = self.black_chat.find({"black_word": True}, no_cursor_timeout=True)
        phrases_list = [black_word_dict["keyword"]
                        for black_word_dict in black_word_dicts]
        black_word_dicts.close()
        for keyword in phrases_list:
            keyword_dict = self.black_chat.find_one({"keyword": keyword})
            mess_id_list = keyword_dict.get(keyword)
            if mess_id_list:
                correct_intervals_list = self.__get_correct_messages_id_for_save(
                    mess_id_list)

                yield keyword, correct_intervals_list

    def save_black_message(self):
        for keyword, elements in self.__get_message_id_with_keyword_in_black_list():
            for element in elements:
                mess_id = self.list_id_mess[element]
                message = self.chat.find_one({"mess_id": mess_id})
                message["chat"] = self.chat_id
                message["keyword"] = keyword
                message['check_person'] = False
                keys_for_del = ["_id", "check_contact", "check_black_word"]
                for key in keys_for_del:
                    message.pop(key)

                self.black_chat.insert_one(message)


class PersonBlackWord(MongoClientForTelegram):

    def __init__(self, chat_id):
        super().__init__()
        self.chat_id = chat_id.replace('@', '')
        name_black_collect = f'black_{self.chat_id}'
        self.black_chat = self.telegram_mess_db[name_black_collect]
        self.persons_card = self.telegram_mess_db["persons_card"]

    def save_person_with_black_message(self):
        def get_keyword_list(keyword):
            if "#" in keyword:
                keyword = keyword.split("#")
            else:
                keyword = [keyword, ]
            return keyword

        black_mess = self.black_chat.find(
            {"type": "message", "check_person": False})
        for message in black_mess:

            person_black_word = dict(
                type="person_black_word",
                chat=message["chat"],
                keyword=get_keyword_list(message["keyword"]),
                name_id=message['name_id'],
            )
            mess_id = message["mess_id"]
            add_mess_id = {
                '$addToSet': {"mess_id": mess_id},
            }
            self.persons_card.update_one(
                person_black_word, add_mess_id, upsert=True)

            self.black_chat.update_one({'type': 'message', "mess_id": mess_id}, {
                                       '$set': {"check_person": True}})


class PersonContact(MongoClientForTelegram):

    TEMPLATE_PHRASE_CONTACT = ['c @', 'к @',
                               'скажи', 'пиши', 'обрати', 'звони']

    def __init__(self,
                 chat_id,
                 TEMPLATE_PHRASE_CONTACT=TEMPLATE_PHRASE_CONTACT):
        super().__init__()
        self.template_phrase_contact = TEMPLATE_PHRASE_CONTACT
        self.chat_id = chat_id.replace('@', '')
        self.chat = self.telegram_mess_db[self.chat_id]
        self.persons_card = self.telegram_mess_db["persons_card"]
        self.telegram_client = InitTelegramClient(self.chat_id)
        self.client = self.telegram_client.start()

    def __get_user_entity(self, key, user_identify):
        # получает открытые данные пользователя по ID или username.
        # key - то что хотим получить.
        if user_identify.isdigit():
            user_identify = int(user_identify)

        # Если пользователь удалил username, возникает ошибка.
        try:
            print("поиск ид по имени...")
            entity = self.client.get_entity(user_identify)
            print(entity.id)
        except:
            print("None")
            return None
        params = dict(
            id=entity.id,
            username=entity.username
        )
        return params[key]

    def __update_contact(self, name_id_mentioned_list, name_id):
        contact_dict = {
            '$addToSet': {"contact": {"$each": name_id_mentioned_list}},
        }
        self.persons_card.update_one(
            {'type': 'person_contact', "name_id": name_id}, contact_dict, upsert=True)
        print(name_id, contact_dict)

    def save_whom_mentioned(self):

        def is_template_in_message(mess):
            for template in self.template_phrase_contact:
                if template in mess.strip().lower():
                    return True

        def save_who_mentioned(name_id_mentioned_list, who_mentioned_id):
            for name_id in name_id_mentioned_list:
                if not isinstance(who_mentioned_id, list):
                    who_mentioned_id = [who_mentioned_id, ]
                self.__update_contact(who_mentioned_id, name_id)

        count = 1
        messages = self.chat.find(
            {'type': 'message', "check_contact": False, "message":{"$regex": " @", "$options":"i"}}, no_cursor_timeout=True)

        for message in messages:
            mess = message["message"]
            if is_template_in_message(mess):
                name_id_mentioned_list = []
                for name in regex.findall(r"\s@\w+\s", mess):
                    if not "_bot" in name:
                        # банят за флуд на сутки. Необходимо каждые 50 запросов делать перерыв более 15 минут.
                        # https://python.gotrained.com/adding-telegram-members-to-your-groups-telethon-python/
                        if (count % 45 == 0):
                            print("Sleep 1000 sec. Waiting...")
                            time.sleep(1000)
                            print("End sleep.")

                        user_id = self.__get_user_entity("id", name.strip())
                        count += 1
                        if user_id:
                            name_id_mentioned_list.append(user_id)

                if name_id_mentioned_list:
                    save_who_mentioned(
                        name_id_mentioned_list, message["name_id"])
                    self.__update_contact(
                        name_id_mentioned_list, message["name_id"])

            self.chat.update_one({'type': 'message', "mess_id": message["mess_id"]}, {
                                 '$set': {"check_contact": True}})
        # Закрываем курсор монго.
        messages.close()
        # Закрываем сессию телеграмм.
        self.telegram_client.stop()


def main():
    #  "@KharkovChat", "@spbbiz", '@ponyrznspb',"@msk24", "@piterchat", "@chat_msk", "@nedimon_msk", "@politach", "")
    value = "@piterchat"
    # Собираем все сообщения чата.
    """ worker = TelegramMessageCollector(chat_id=value)
    worker.start_collector() """
    # Ищем в сообщениях упоминания других персон и создаем или дописываем карточки персон.
    person_contact = PersonContact(chat_id=value)
    person_contact.save_whom_mentioned()
    # Собираем все сообщения содерщащие слова из черного списка +- 5
    """ filter_black_word = FilterMessagesByBlackList(chat_id=value)
    filter_black_word.save_black_message() """
    # Ищем в сообщениях со словами из черного списка ИД персон и создаем или дописываем карточки информацией с номерами сообщений и фразами ,которые там упоминались.
    """ person_black_word = PersonBlackWord(chat_id=value)
    person_black_word.save_person_with_black_message() """


if __name__ == '__main__':
    main()


# db.msk24.find({"type" : "message", $text:{$search:"купить"}}) - полнотекстовый поиск

# db.spbbiz.createIndex({"message" : "text"}, { default_language: "russian" }) - индексация текста
