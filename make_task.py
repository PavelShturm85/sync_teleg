import pika
import json


class MakeNewTasks():

    def __init__(self, queue='telegram_message_collector'):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost', port=5672))
        channel = connection.channel()
        self.queue = queue
        channel.queue_declare(queue=self.queue, durable=True)
        self.channel = channel
        self.__current_method = dict(
            telegram_watch_dog=self.__create_telegram_message_collector_task,
        )

    def __create_telegram_message_collector_task(self, value):
        task = {"value": value}
        self.__add_task(task)

    def __add_task(self, task):
        self.channel.basic_publish(exchange='',
                                   routing_key=self.queue,
                                   body=json.dumps(task),
                                   properties=pika.BasicProperties(
                                       delivery_mode=2,)
                                   )

    def create_task(self, value):
        if value and isinstance(value, str):
            self.__current_method[self.queue](value)


if __name__ == "__main__":
    # "@rospres", "@economika", "@aavst55", "@navalny"
    values = "@ctfcup18"
    add_task = MakeNewTasks(queue='telegram_message_collector')
    add_task.create_task(values)
