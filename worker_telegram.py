import time
import json
import pika
from telegram_message_collector import TelegramMessageCollector

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='telegram_message_collector', durable=True)

print(' [*] Waiting for messages to telegram_watch_dog. To exit press CTRL+C')


def callback(ch, method, properties, body):
    body = json.loads(body)
    value = body.get("value", "")
    if value:
        print(f"### value: {value}")
        worker = TelegramMessageCollector(value)
        worker.start_collector()
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='telegram_message_collector')

channel.start_consuming()
